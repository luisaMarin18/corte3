const { Client } = require('whatsapp-web.js');
const express = require('express');
const bodyParser = require('body-parser');
const qrcode = require('qrcode-terminal');
const nodemailer = require('nodemailer');
const { MongoClient } = require('mongodb');

const app = express();
const port = 3000;

const client = new Client();
const delay = ms => new Promise(res => setTimeout(res, ms));

const uri = 'BD_Corte 03'; 

async function run() {
  client.on('qr', qr => {
    qrcode.generate(qr, { small: true });
  });

  client.on('ready', () => {
    console.log('¡Bien! WhatsApp conectado.');
  });

  await client.initialize();
}

function cumprimentar() {
  const dataAtual = new Date();
  const hora = dataAtual.getHours();

  let saudacao;

  if (hora >= 6 && hora < 12) {
    saudacao = "Buenos días";
  } else if (hora >= 12 && hora < 17) {
    saudacao = "Buenas tardes";
  } else {
    saudacao = "Buenas noches";
  }

  return saudacao;
}

async function enviarSaludo(numero) {
  const saudacoes = cumprimentar();
  await client.sendMessage(numero, saudacoes);
}

async function enviarCorreo(numero) {
  const toEmail = 'juanitabella@gamil.com'; 
  const subject = 'Imformacion Solicitad';
  const text = 'La informacion solcita es de caracter personal';

  const mailOptions = {
    from: 'juanitorevibe@gamil.com';
    to: 'juanitorevibe@gamil.com';
    subject: 'Imformacion Solicitada';
    text: 'La informacion solcita es de caracter personal';
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('E-mail enviado: ' + info.response);
      const successMessage = 'E-mail enviado satisfactoriamente! 📧';
      client.sendMessage(numero, successMessage);
    }
  });
}

async function obtenerInformacion(cedula) {
  const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

  try {
    await client.connect();
    const database = client.db('bd bot');
    const collection = database.collection('Datos_Alamacenados');

    const resultado = await collection.findOne({ cedula: cedula });

    if (resultado) {
      return resultado.informacion;
    } else {
      return 'No se ha encontrado información para la cédula solicitada';
    }
  } finally {
    await client.close();
  }
}

function waitForResponse() {
  return new Promise((resolve, reject) => {
    client.on('message', async msg => {
      if (msg.from.endsWith('@c.us')) {
        resolve(msg);
      }
    });
  });
}

// esta es la configurar del middleware para analizar el cuerpo de la solicitud en formato JSON
app.use(bodyParser.json());

// Ruta para manejar las solicitudes POST del webhook
app.post('/webhook', async (req, res) => {
  const msg = req.body;

  if (msg.body) {
    const lowerCaseMessage = msg.body.toLowerCase();

    if (lowerCaseMessage.includes('opcion 1')) {
      await enviarSaludo(msg.from);
    } else if (lowerCaseMessage.includes('opcion 2')) {
      await client.sendMessage(msg.from, 'Por favor, proporcionar la cédula:');
      const response = await waitForResponse();
      const cedulaIngresada = response.body;
      const informacion = await obtenerInformacion(cedulaIngresada);
      await client.sendMessage(msg.from, `Información asociada a ${cedulaIngresada}: ${informacion}`);
    } else {
      await client.sendMessage(msg.from, 'Por favor, elija una opción válida (opcion 1 u opcion 2).');
    }
  }

  res.end();
});

// Iniciar el servidor Express
app.listen(port, () => {
  console.log(`Servidor web escuchando en http://localhost:${port}`);
});

// Iniciar el bot de WhatsApp
run().catch(err => console.error(err));